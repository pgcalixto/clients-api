'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    ativo: {type: Boolean, required: true},
    idGrupo: {type: String, required: true},
    cpf: {type: String, required: true, index: true},
    email: {type: String, required: true},
    senha: {type: String, required: true},
    senhaReserva: String,
    tokenSessao: String,
    tokenRegistro: String,
    nome: {type: String, required: true},
    dataDeNascimento: {type: String, required: true},
    telefone: {type: String, required: true},
    enderecos: {type: [{
      ativo: {type: Boolean, required: true},
      id: {type: String, required: true},
      numeroCasa: {type: Number, required: true},
      complemento: String,
      referencia: String
    }], required: true},
});

var UserModel = mongoose.model('users', UserSchema);
module.exports = UserModel;
