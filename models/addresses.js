'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var AddressSchema = new Schema({
    cep: {type: String, required: true},
    rua: {type: String, required: true},
    bairro: {type: String, required: true},
    cidade: {type: String, required: true},
    estado: {type: String, required: true}
});

var AddressModel = mongoose.model('addresses', AddressSchema);
module.exports = AddressModel;
