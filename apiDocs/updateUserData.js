/**
* @api {put} /users/{cpf}/update Altera dados cadastrais de um usuário
* @apiName Alteração de dados cadastrais
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
* @apiParam {String} [nome] Nome a ser alterado.
* @apiParam {String} [email] Email a ser alterado.
* @apiParam {String} [dataDeNascimento] Data de nascimento a ser alterada.
* @apiParam {String} [telefone] Telefone a ser alterado.
*
* @apiSuccess {String} nome Novo nome, se requisitada alteração, ou nome já
existente.
* @apiSuccess {String} email Novo email, se requisitada alteração, ou email já
existente.
* @apiSuccess {String} dataDeNascimento Nova data de nascimento, se requisitada
alteração, ou data já existente.
* @apiSuccess {String} telefone Novo telefone, se requisitada alteração, ou
telefone já existente.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "nome": "Natália Jennifer Isadora da Cunha",
  "email": "nataliajc@pozzer.net",
  "data_nasc": "18/07/1982",
  "telefone": "+5586992611306"
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Não há usuário cadastrado com o CPF fornecido."
}
*/
