define({ "api": [
  {
    "type": "post",
    "url": "/{cpf}/addresses",
    "title": "Adiciona endereço",
    "name": "Adi__o_de_endere_o_para_um_usu_rio",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cep",
            "description": "<p>CEP do endereço.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "rua",
            "description": "<p>Rua do endereço.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numeroCasa",
            "description": "<p>Número da casa.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "complemento",
            "description": "<p>Complemento do endereço.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "referencia",
            "description": "<p>Ponto de referência do endereço.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "bairro",
            "description": "<p>Bairro do endereço.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cidade",
            "description": "<p>Cidade do endereço.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "estado",
            "description": "<p>Estado do endereço.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID do endereço.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"CEP inválido! Apenas 8 dígitos permitidos.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Número inválido!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Sua sessão expirou! Por favor, faça login novamente.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Usuário não encontrado.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/addAddress.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "put",
    "url": "/{cpf}/update",
    "title": "Altera dados cadastrais de um usuário",
    "name": "Altera__o_de_dados_cadastrais",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "dado",
            "description": "<p>Dado a ser alterado - name, email, dateOfBirth ou phone.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\tHTTP/1.1 200 OK\n  {\n  \"nome\": \"Natália Jennifer Isadora da Cunha\",\n  \"email\": \"nataliajc@pozzer.net,\n  \"data_nasc\": \"18/07/1982\",\n  \"telefone\": \"+5586992611306\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Não há usuário cadastrado com o CPF fornecido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/updateUserData.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Autentica um usuário",
    "name": "Autentica__o_de_usu_rios_",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "senha",
            "description": "<p>Senha do usuário (hash).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Content-type deve ser application/json.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Não foi fornecido e-mail ou senha.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Senha inválida.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"E-mail inválido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/authUser.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/delete",
    "title": "Exclui conta",
    "name": "Exclus_o_de_conta",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "senha",
            "description": "<p>Senha do usuário (hash).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"message\": \"Conta excluída com sucesso.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Token de sessão ou senha não informado(s).\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Senha inválida.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Token de sessão inválido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/excludeAccount.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/{cpf}/logout",
    "title": "Efetua logout de usuário",
    "name": "Logout_de_usu_rio",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"message\": \"Logout efetuado com sucesso.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Content-type deve ser application/json.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Não foi fornecido token de sessão.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Token de sessão inválido.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Nenhum usuário com o CPF fornecido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/logout.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/users/{cpf}",
    "title": "Obtem dados de usuário",
    "name": "Obten__o_de_dados_de_usu_rio",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"email\": \"nataliajc@pozzer.net\",\n\"nome\": \"Natália Jennifer Isadora da Cunha\",\n\"data_nasc\": \"18/07/1982\",\n\"telefone\": \"+5586992611306\",\n\"idGrupo\": \"ouisdyo7yh0f8od7\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Token de sessão não fornecido.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Token de sessão inválido.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Não há usuário cadastrado com o CPF fornecido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/getUserData.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "get",
    "url": "/{cpf}/addresses",
    "title": "Obtém os endereços de um usuário",
    "name": "Obten__o_dos_endere_os_de_usu_rios",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n  \"id\": \"224453fc4-077e-44c5-b6c6-f9be2df35b6\",\n  \"cep\": \"64606068\",\n  \"rua\": \"Avenida Marques de Medeiros\",\n  \"numeroCasa\": \"123\",\n  \"bairro\": \"Jardim Natal\",\n  \"complemento\": \"\",\n  \"referencia\": \"Casa de esquina, com portão verde\",\n  \"cidade\": \"Picos\",\n  \"estado\": \"PI\"\n  },\n  {\n  \"id\": \"3369dae2-8f45-4275-8d1f-71796d74deff\",\n  \"cep\": \"69914374\",\n  \"rua\": \"Rua Cruzeiro do Sul\",\n  \"numeroCasa\": \"321\",\n  \"bairro\": \"Calafate\",\n  \"complemento\": \"Apartamento 122\",\n  \"referencia\": \"\",\n  \"cidade\": \"Picos\",\n  \"estado\": \"PI\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Não há usuário cadastrado com o CPF fornecido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/getUserAddress.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/{cpf}/recover",
    "title": "Recupera senha de usuário",
    "name": "Recupera__o_de_senha",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "senha",
            "description": "<p>Senha do usuário (hash).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/recover.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Registra um usuário",
    "name": "Registro_de_usu_rios___parte_1",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cpf",
            "description": "<p>CPF do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email do usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "senha",
            "description": "<p>Senha do usuario (hash).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nome",
            "description": "<p>Nome completo do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dataDeNascimento",
            "description": "<p>Data de nascimento do usuário.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "idGrupo",
            "description": "<p>ID do grupo a que o usuário pertence.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefone",
            "description": "<p>Telefone do usuário.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "registerToken",
            "description": "<p>Token de registro do usuario.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Dado não fornecido: {dado}\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/registerUser1.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/confirm",
    "title": "Confirma registro de um usuário",
    "name": "Registro_de_usu_rios___parte_2",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "registerToken",
            "description": "<p>Token de registro do usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"sessionToken\": \"pedropater@gmail.com:1537646189194\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Token de registro não informado.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Token de registro inválido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/registerUser2.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "delete",
    "url": "/{cpf}/addresses",
    "title": "Remove endereço",
    "name": "Remo__o_de_endere_o",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID do endereço.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de sucesso.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\nmessage: \"Endereço removido com sucesso.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Sua sessão expirou! Por favor, faça login novamente.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Usuário não encontrado.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Endereço não encontrado.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/removeAddress.js",
    "groupTitle": "Clients_API"
  },
  {
    "type": "post",
    "url": "/users/{cpf}/logged",
    "title": "Valida um token de sessão",
    "name": "Verifica__o_de_validade_de_token_",
    "group": "Clients_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenSessao",
            "description": "<p>Token de sessão.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"message\": \"Usuário logado.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mensagem de erro.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n\"message\": \"Token de sessão não informado.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\"message\": \"Token de sessão inválido.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response",
          "content": "HTTP/1.1 404 Not Found\n{\n\"message\": \"Não há usuário cadastrado com o CPF fornecido.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "apiDocs/validateToken.js",
    "groupTitle": "Clients_API"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "apiDocs/docs-site/main.js",
    "group": "_home_ec2015_ra171836_MC851_clients_api_apiDocs_docs_site_main_js",
    "groupTitle": "_home_ec2015_ra171836_MC851_clients_api_apiDocs_docs_site_main_js",
    "name": ""
  }
] });
