/**
* @api {post} /addresses/{cpf}/add Adiciona endereço
* @apiName Adição de endereço para um usuário
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão do usuário.
* @apiParam {String} cep CEP do endereço.
* @apiParam {String} rua Rua do endereço.
* @apiParam {String} numeroCasa Número da casa.
* @apiParam {String} [complemento] Complemento do endereço.
* @apiParam {String} [referencia] Ponto de referência do endereço.
* @apiParam {String} bairro Bairro do endereço.
* @apiParam {String} cidade Cidade do endereço.
* @apiParam {String} estado Estado do endereço.
*
* @apiSuccess {String} id ID do endereço.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "id": "5bd0fa1366eafa7f96fbde31"
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "CEP inválido! Apenas 8 dígitos permitidos."
}
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Número inválido!"
}
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Sua sessão expirou! Por favor, faça login novamente."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Usuário não encontrado."
}
*/
