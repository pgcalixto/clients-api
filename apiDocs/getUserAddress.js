/**
* @api {get} /users/{cpf}/addresses Obtém os endereços de um usuário
* @apiName Obtenção dos endereços de usuários
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
*
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

[
  {
    "id": "224453fc4-077e-44c5-b6c6-f9be2df35b6",
    "cep": "64606068",
    "rua": "Avenida Marques de Medeiros",
    "numeroCasa": "123",
    "bairro": "Jardim Natal",
    "complemento": "",
    "referencia": "Casa de esquina, com portão verde",
    "cidade": "Picos",
    "estado": "PI"
  },
  {
    "id": "3369dae2-8f45-4275-8d1f-71796d74deff",
    "cep": "69914374",
    "rua": "Rua Cruzeiro do Sul",
    "numeroCasa": "321",
    "bairro": "Calafate",
    "complemento": "Apartamento 122",
    "referencia": "",
    "cidade": "Picos",
    "estado": "PI"
  }
]
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Não há usuário cadastrado com o CPF fornecido."
}
*/
