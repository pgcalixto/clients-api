/**
* @api {delete} /addresses/{cpf}/remove Remove endereço
* @apiName Remoção de endereço
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
* @apiParam {String} id ID do endereço.
*
* @apiSuccess {String} message Mensagem de sucesso.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  message: "Endereço removido com sucesso."
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Sua sessão expirou! Por favor, faça login novamente."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Usuário não encontrado."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Endereço não encontrado."
}
*/
