/**
* @api {post} /users/{cpf}/logged Valida um token de sessão
* @apiName Verificação de validade de token.
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
*
* @apiSuccess {String} message Mensagem de sucesso.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "message": "Usuário logado."
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Token de sessão não informado."
}
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Token de sessão inválido."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Não há usuário cadastrado com o CPF fornecido."
}
*/
