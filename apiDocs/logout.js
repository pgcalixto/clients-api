/**
* @api {post} /{cpf}/logout Efetua logout de usuário
* @apiName Logout de usuário
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
*
* @apiSuccess {String} message Mensagem de sucesso.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "message": "Logout efetuado com sucesso."
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Content-type deve ser application/json."
}
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Não foi fornecido token de sessão."
}
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Token de sessão inválido."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Nenhum usuário com o CPF fornecido."
}
*/
