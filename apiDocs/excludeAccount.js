/**
* @api {post} /delete Exclui conta
* @apiName Exclusão de conta
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
* @apiParam {String} email Email do usuário.
* @apiParam {String} senha Senha do usuário (hash).
*
* @apiSuccess {String} message Mensagem de sucesso.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "message": "Conta excluída com sucesso."
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Token de sessão ou senha não informado(s)."
}
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Senha inválida."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Token de sessão inválido."
}
*/
