/**
* @api {post} /login Autentica um usuário
* @apiName Autenticação de usuários.
* @apiGroup Clients API
*
* @apiParam {String} email Email do usuário.
* @apiParam {String} senha Senha do usuário (hash).
*
* @apiSuccess {String} tokenSessao Token de sessão.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "tokenSessao": "pedropater@gmail.com:1592650180194"
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Content-type deve ser application/json."
}
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Não foi fornecido e-mail ou senha."
}
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Senha inválida."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "E-mail inválido."
}
*/
