/**
* @api {post} /register Registra um usuário
* @apiName Registro de usuários - parte 1
* @apiGroup Clients API
*
* @apiParam {String} cpf CPF do usuário.
* @apiParam {String} email Email do usuário.
* @apiParam {String} senha Senha do usuário (hash).
* @apiParam {String} nome Nome completo do usuário.
* @apiParam {String} dataDeNascimento Data de nascimento do usuário.
* @apiParam {String} idGrupo ID do grupo a que o usuário pertence.
* @apiParam {String} telefone Telefone do usuário.
*
* @apiSuccess {String} registerToken Token de registro do usuário.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "registerToken": "pedropater@gmail.com:1532636189004"
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Dado não fornecido: {dado}"
}
*/
