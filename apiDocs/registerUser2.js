/**
* @api {post} /confirm Confirma registro de um usuário
* @apiName Registro de usuários - parte 2
* @apiGroup Clients API
*
* @apiParam {String} registerToken Token de registro do usuário.
*
* @apiSuccess {String} sessionToken Token de sessão.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "sessionToken": "pedropater@gmail.com:1537646189194"
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Token de registro não informado."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Token de registro inválido."
}
*/
