/**
* @api {post} /users/{cpf} Obtem dados de usuário
* @apiName Obtenção de dados de usuário
* @apiGroup Clients API
*
* @apiParam {String} tokenSessao Token de sessão.
*
* @apiSuccess {String} email Email do usuário.
* @apiSuccess {String} nome Nome do usuário.
* @apiSuccess {String} dataDeNascimento Data de nascimento do usuário.
* @apiSuccess {String} telefone Telefone do usuário.
* @apiSuccess {String} idGrupo ID do grupo ao qual o usuário pertence.
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK

{
  "email": "nataliajc@pozzer.net",
  "nome": "Natália Jennifer Isadora da Cunha",
  "data_nasc": "18/07/1982",
  "telefone": "+5586992611306",
  "idGrupo": "ouisdyo7yh0f8od7"
}
*
* @apiError {String} message Mensagem de erro.
* @apiErrorExample Error-Response
HTTP/1.1 400 Bad Request

{
  "message": "Token de sessão não fornecido."
}
* @apiErrorExample Error-Response
HTTP/1.1 401 Unauthorized

{
  "message": "Token de sessão inválido."
}
* @apiErrorExample Error-Response
HTTP/1.1 404 Not Found

{
  "message": "Não há usuário cadastrado com o CPF fornecido."
}
*/
