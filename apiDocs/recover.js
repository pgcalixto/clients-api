/**
* @api {post} /{cpf}/recover Recupera senha de usuário
* @apiName Recuperação de senha
* @apiGroup Clients API
*
* @apiParam {String} email Email do usuário.
* @apiParam {String} senha Senha do usuário (hash).
*
* @apiSuccessExample Success-Response
HTTP/1.1 200 OK
*/
