'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users');

/* POST confirm new user registration */
router.post('/', function (req, res, next) {

  var registerToken = req.body.registerToken;

  if (registerToken === undefined) {
    res.status(400).send({
      message: "Token de registro não informado."
    });
    return;
  }

  UserModel.findOne({tokenRegistro: registerToken},
                    {_id: 0, email: 1})
  .then(function(user) {
    if (user == null) {
        res.status(404).send({
          message: "Token de registro inválido."
        });
        return;
    }

    // update user as active, set sessionToken
    UserModel.findOneAndUpdate({tokenRegistro: registerToken},
                               {$set: {tokenRegistro: null, ativo: true}})
    .then(function(user) {
      res.status(200).send({
        message: "Usuário confirmado com sucesso. " +
                 "Faça seu login para acessar a conta."
      });
      return;
    })
    .catch(function(err) {
      res.status(500).send({
        message: err.message || "Erro no banco de dados interno."
      });
      return;
    });
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
