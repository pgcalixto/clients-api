'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users');
const AddressModel = require('../models/addresses');

/* GET user by CPF */
router.post('/:cpf', function(req, res, next) {

  var cpf = req.params.cpf;
  var tokenSessao = req.body.tokenSessao;

  if(tokenSessao === undefined) {
    res.status(400).send( {
      message: "Token de sessão não fornecido."
    });
    return;
  }

  UserModel.findOne({cpf: cpf},
                    {_id: 0, ativo: 1, tokenSessao: 1, nome: 1, email: 1,
                     dataDeNascimento: 1, telefone: 1, idGrupo: 1})
  .then(function(user) {

    if (user == null) {
      res.status(404).send({
        message:
          "Não há usuário cadastrado com o CPF fornecido."
      });
      return;
    }

    if (user.tokenSessao !== tokenSessao) {
      res.status(401).send({
        message: "Token de sessão inválido."
      });
      return;
    }

    delete user.tokenSessao;
    res.send(user);
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

/* GET user addresses by CPF */
router.get('/:cpf/addresses', function(req, res, next) {
  UserModel.findOne({cpf: req.params.cpf},
                    {_id: 0, enderecos: 1})
  .then(function(user) {
    if (user == null) {
      res.status(404).send({
        message: "Não há usuário cadastrado com o CPF fornecido."
      });
      return;
    }

    /* For each user address (made of an id, house number, complement and
     * refference, create a promise that's a query to the DB for the full
     * address information (street name, district, etc.) */
    var enderecosColetados = {};
    var promises = [];
    for (var i = 0; i < user.enderecos.length; i++) {
      var address = user.enderecos[i]
      if (address.ativo == true) {
        var promise = AddressModel.findById(address.id)
        promise.catch(function(err) {
          res.status(500).send({
            message: err.message || "Erro no banco de dados interno."
          });
          return;
        });

        promises.push(promise);
      }
    }

    /* Resolve ao promises (queries), and map the received data using addresses 
     * ids as keys - since the promises are asynchronous, it will be ineficient 
     * to use an array when assembling the full address information with 
     * specific ones */
    Promise.all(promises)
    .then(function(data) {
      for (var i = 0; i < data.length; i++) {
        enderecosColetados[data[i]._id] = {
          id: data[i]._id,
          cep: data[i].cep,
          rua: data[i].rua,
          bairro: data[i].bairro,
          cidade: data[i].cidade,
          estado: data[i].estado
        };
      }

      /* Assemble the full address information with specific ones and return
       * all of them */
      var enderecos = []
      for (var i = 0; i < user.enderecos.length; i++) {
        var fullAddressId = user.enderecos[i].id;
        if (user.enderecos[i].ativo == true) {
          enderecos.push({
            id: user.enderecos[i]._id,
            cep: enderecosColetados[fullAddressId].cep,
            rua: enderecosColetados[fullAddressId].rua,
            numeroCasa: user.enderecos[i].numeroCasa,
            bairro: enderecosColetados[fullAddressId].bairro,
            complemento: user.enderecos[i].complemento,
            referencia: user.enderecos[i].referencia,
            cidade: enderecosColetados[fullAddressId].cidade,
            estado: enderecosColetados[fullAddressId].estado
          });
        }
      }

      res.status(200).send(enderecos);
    })
    .catch(function(err) {
      res.status(500).send({
        message: err.message || "Erro no banco de dados interno."
      });
      return;
    })
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

/* POST is user logged? */
router.post('/:cpf/logged', function (req, res, next) {
  var tokenSessao = req.body.tokenSessao;

  if (tokenSessao === undefined) {
    res.status(400).send({
      message: "Token de sessão não informado."
    });
    return;
  }
  UserModel.findOne({cpf: req.params.cpf},
                    {_id: 0, tokenSessao: 1})
  .then(function(user) {

    if (user == null) {
      res.status(404).send({
        message: "Não há usuário cadastrado com o CPF fornecido."
      });
      return;
    }

    if (tokenSessao != user.tokenSessao) {
      res.status(401).send({
        message: "Token de sessão inválido."
      });
      return;
    }

    res.send({
      message: "Usuário logado."
    });
    return;
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

/* GET get users by group */
router.get('/groups/:groupId/', function (req, res, next) {
  UserModel.find({idGrupo: req.params.groupId})
  .then(function(users) {
    if (users.length === 0) {
      res.status(404).send( {
        message: "Não há usuários com o ID de grupo fornecido."
      });
      return;
    }
    res.status(200).send(users);
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

router.put('/:cpf/update', function (req, res, next) {
  var name;
  var email;
  var dateOfBirth;
  var phone;

  UserModel.findOne({ cpf: req.params.cpf })
  .then(function(user) {
    if (user != null) {
      name = req.body.nome !== undefined ?
        req.body.nome :
        user.nome;
      email = req.body.email !== undefined ?
        req.body.email :
        user.email;
      dateOfBirth = req.body.dataDeNascimento !== undefined ?
        req.body.dataDeNascimento :
        user.dataDeNascimento;
      phone = req.body.telefone !== undefined ?
        req.body.telefone :
        user.telefone;

      UserModel.findOneAndUpdate({ cpf: req.params.cpf },
        { $set:{nome: name, email: email, dataDeNascimento: dateOfBirth, telefone: phone}})
        .then(function (user) {
          res.send(user);
        })
        .catch(function (err) {
          res.status(500).send({
            message: err.message || "Erro no banco de dados interno."
          });
        });
    } else {
      res.status(404).send({
        message: "Não há usuário cadastrado com o CPF fornecido."
      });
      return;
    }
  }).catch(function (err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
