function isNumber(string) {
  return /^\d+$/.test(string);
}

function verifyAddress(cep, rua, numeroCasa, bairro, cidade, estado) {
  if (cep.length != 8 || !isNumber(cep)) {
    return "CEP inválido! Apenas 8 dígitos permitidos.";
  }
  // TODO: validação da rua usando API.
  if (!isNumber(numeroCasa)) {
      return "Número inválido!";
  }
  // TODO: validação do bairro usando API.
  // TODO: validação da cidade usando API.
  // TODO: validação do estado usando API.
  return "";
}

module.exports = {
  verifyAddress: verifyAddress
};