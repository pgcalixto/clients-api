'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users.js');

/* POST logout user */
router.post('/:cpf', function(req, res, next) {

  if (req.headers['content-type'] !== 'application/json') {
    res.status(400).send({
      message: "Content-type deve ser application/json."
    });
    return;
  }

  var tokenSessao = req.body.tokenSessao;

  if (tokenSessao === undefined) {
    res.status(400).send({
      message: "Não foi fornecido token de sessão."
    });
    return;
  }
  var cpf = req.params.cpf;
  UserModel.findOne({cpf: cpf},
                    {_id: 0, tokenSessao: 1})
  .then(function(user) {
    if (user == null) {
        res.status(404).send({
          message: "Nenhum usuário com o CPF fornecido."
        });
        return;
    }
    if (user.tokenSessao !== tokenSessao) {
      res.status(401).send({
        message: "Token de sessão inválido."
      });
      return;
    }
    UserModel.findOneAndUpdate({cpf: cpf},
                               {$set: {tokenSessao: null}})
    .then(function(user) {
      res.status(200).send({
        message: "Logout efetuado com sucesso."
      });
    })
    .catch(function(err) {
      res.status(500).send({
        message: err.message || "Erro no banco de dados interno."
      });
    });
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
