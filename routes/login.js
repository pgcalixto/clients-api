'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users.js');

/* POST authenticate user */
router.post('/', function(req, res, next) {

  if (req.headers['content-type'] != 'application/json') {
    res.status(400).send({
      message: "Content-type deve ser application/json."
    });
    return;
  }

  var email = req.body.email;
  var password = req.body.senha;

  if (email == null || password == null) {
    res.status(400).send({
      message: "Não foi fornecido e-mail ou senha."
    });
    return;
  }

  UserModel.findOne({email: email},
                    {_id: 0, email: 1, senha: 1})
  .then(function(user) {
    if (user == null) {
        res.status(404).send({
          message: "E-mail inválido."
        });
        return;
    }

    if (user.senha != password) {
      res.status(401).send({
        message: "Senha inválida."
      });
      return;
    }

    /* If password is correct, create and return session token */
    var currentDate = new Date();
    var timeStamp = currentDate.getTime().toString();
    var token = email + ":" + timeStamp;

    /* Update database with session token */
    UserModel.findOneAndUpdate({email: email}, {$set: {tokenSessao: token}})
    .then(function(user) {
      console.log(user);
      res.send({
        sessionToken: token
      });
      return;
    })
    .catch(function(err) {
      res.status(500).send({
        message: err.message || "Erro no banco de dados interno."
      });
      return;
    });
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
