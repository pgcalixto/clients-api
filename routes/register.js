'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users');

/* POST new user */
router.post('/', function (req, res, next) {

  // verify request data
  var required = ['cpf', 'email', 'senha', 'nome', 'dataDeNascimento','idGrupo',
                  'telefone'];

  for(let i = 0; i < required.length; i++) {
    if (req.body[required[i]] === undefined) {
      res.status(400).send({
        message: "Dado não fornecido: " + required[i]
      });
      return;
    }
  }

  UserModel.findOne({email: req.body.email})
  .then(function (user) {
    if (user != null) {
      res.status(409).send({
        message: "Email inserido já está em uso!"
      });
    } else {
      UserModel.findOne({cpf: req.body.cpf})
      .then(function (user) {
        if (user != null) {
          res.status(409).send({
            message: "CPF inserido já está em uso!"
          });
        } else {
          // create new user
          var date = new Date();
          var registerToken = req.body.email + ':' + date.getTime().toString();
          var user = new UserModel({
            ativo: false,
            idGrupo: req.body.idGrupo,
            cpf: req.body.cpf,
            email: req.body.email,
            senha: req.body.senha,
            nome:  req.body.nome,
            dataDeNascimento: req.body.dataDeNascimento,
            telefone: req.body.telefone,
            tokenRegistro: registerToken,
            enderecos: []
          });

          user.save()
          .then(function(data) {
            res.send({
              registerToken: registerToken,
            });
          });
        }
      });
    }
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;