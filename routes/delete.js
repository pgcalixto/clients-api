'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users.js');

/* POST delete user */
router.post('/', function(req, res, next) {

  var tokenSessao = req.body.tokenSessao;
  var senha = req.body.senha;
  if (tokenSessao === undefined || senha == undefined) {
    req.status(400).send({
      message: "Token de sessão ou senha não informado(s)."
    });
    return;
  }
  UserModel.findOne({tokenSessao: tokenSessao},
                    {_id: 0, senha: 1})
  .then(function(user) {
    if (user == null) {
        res.status(404).send({
          message: "Token de sessão inválido."
        });
        return;
    }

    if (user.senha != senha) {
      res.status(401).send({
        message: "Senha inválida."
      });
      return;
    }

    UserModel.findOneAndDelete({tokenSessao: tokenSessao})
    .then(function(user) {
      res.status(200).send({
        message: "Conta excluída com sucesso."
      });
      return;
    })
    .catch(function(err) {
      res.status(500).send({
        message: err.message || "Erro no banco de dados interno."
      });
      return;
    });
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
