'use strict';
var express = require('express');
var router = express.Router();

const UserModel = require('../models/users');
const AddressModel = require('../models/addresses');
const AddressValidator = require('./validation/address');

/* GET all addresses */
router.get('/', function(req, res) {
  AddressModel.find()
  .then(function(addresses) {
    res.send(addresses);
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message ||
        "Some error occurred while retrieving addresses from the database."
    });
    return;
  });
});

/* POST new address */
router.post('/:cpf/add', function(req, res, next) {
  var cpf = req.params.cpf;
  var tokenSessao = req.body.tokenSessao;
  var cep = req.body.cep;
  var rua = req.body.rua;
  var numeroCasa = req.body.numeroCasa;
  var complemento = req.body.complemento != null ? req.body.complemento : "";
  var referencia = req.body.referencia != null ? req.body.referencia : "";
  var bairro = req.body.bairro;
  var cidade = req.body.cidade;
  var estado = req.body.estado;

  UserModel.findOne({cpf: cpf},
                    {_id: 0, cpf: 1, tokenSessao: 1, enderecos: 1})
  .then(function(user) {
    if (user == null) {
      res.status(404).send({
        message: "Usuário não encontrado."
      });
      return;
    }

    /* If informed session token doesn't match DB's one, return "unauthorized"
       response */
    if (tokenSessao != user.tokenSessao) {
      res.status(401).send({
        message: "Sua sessão expirou! Por favor, faça login novamente."
      });
      return;
    }

    /* If user data is ok, verify address data */
    var errorMessage = AddressValidator.verifyAddress(cep, rua, numeroCasa,
                       bairro, cidade, estado);
    /* If address data is not ok, return "validation failed" response with
       custom message according to wrong field */
    if (errorMessage !== "") {
      res.status(400).send({
        message: errorMessage
      });
      return;
    }

    AddressModel.findOne({cep: cep})
    .then(function(address) {
      if (address == null) {
        /* If address isn't found, insert a new entry for it into the DB and
           update user's information */
        var newAddress = new AddressModel ({
          cep: cep,
          rua: rua,
          bairro: bairro,
          cidade: cidade,
          estado: estado
        });

        newAddress.save(function(err, results) {
          if (err != null) {
            res.status(500).send({
              message: err.message || "Erro no banco de dados interno."
            });
            return;
          }

          var userAddress = {id: results._id, ativo: true, numeroCasa:
                             numeroCasa, complemento: complemento, referencia:
                             referencia};
          UserModel.findOneAndUpdate({cpf: cpf}, {$push: {enderecos:
                                     userAddress}})
          .then(function(user) {
            res.status(200).send({
              id: results._id
            });
            return;
          });
        });
      } else {
        /* If found, just obtain its unique id and update user's information */
        var userAddress = {id: address._id, ativo: true, numeroCasa: numeroCasa,
                           complemento: complemento, referencia: referencia};
        UserModel.findOneAndUpdate({cpf: cpf}, {$push: {enderecos: userAddress}})
        .then(function(user) {
          res.status(200).send({
            id: address._id
          });
          return;
        }).catch(function(err) {
          res.status(500).send({
            message: err.message || "Erro no banco de dados interno."
          });
          return;
        });
      }
    }).catch(function(err) {
      res.status(500).send({
        message: err.message || "Erro no banco de dados interno."
      });
      return;
    });
  }).catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
    return;
  });
});

/* DELETE existent address */
router.delete('/:cpf/remove', function(req, res, next) {
  var cpf = req.params.cpf;
  var tokenSessao = req.body.tokenSessao;
  var id = req.body.id;

  UserModel.findOne({cpf: cpf},
                    {_id: 0, cpf: 1, tokenSessao: 1, enderecos: 1})
  .then(function(user) {
    if (user == null) {
      res.status(404).send({
        message: "Usuário não encontrado."
      });
      return;
    }

    /* If informed session token doesn't match DB's one, return "unauthorized"
       response */
    if (tokenSessao != user.tokenSessao) {
      res.status(401).send({
        message: "Sua sessão expirou! Por favor, faça login novamente."
      });
      return;
    }

    /* If the user is found, search and remove the desired address using filter
       function - it was easier than remove directly on DB (using $set or other
       options) */
    var oldAddresses = JSON.parse(JSON.stringify(user.enderecos));
    var newAddresses = user.enderecos.filter(function (address) {
      if (address._id == id) {
        address.ativo = false;
      }
      return true;
    })

    /* Compare if the address was succesfully deleted (field "ativo" will be
       different), and remove it (by updating the old user's addresses array
       with the changed one) */
    if (newAddresses.every(function(e, i) {
      return e.ativo == oldAddresses[i].ativo;
    })) {
      console.log("Unchanged");
      res.status(404).send({
        message: "Endereço não encontrado."
      });
      return;
    } else {
      console.log("Changed");
      UserModel.findOneAndUpdate({cpf: cpf}, {$set: {enderecos: newAddresses}})
      .then(function(user) {
        res.status(200).send({
          message: "Endereço removido com sucesso."
        });
        return;
      }).catch(function(err) {
        res.status(500).send({
          message: err.message || "Erro no banco de dados interno."
        });
        return;
      });
    };
  }).catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
    return;
  });
});

module.exports = router;
